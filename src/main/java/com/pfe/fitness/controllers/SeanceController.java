package com.pfe.fitness.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pfe.fitness.entities.Seance;
import com.pfe.fitness.services.SeanceService;


@RestController
@RequestMapping("/seance") // localhost:8087/seance

public class SeanceController {
	
	@Autowired
	private SeanceService seanceService ;

	@GetMapping(path="/all")
	public List<Seance> getAllSeance() {
		return seanceService.getAllSeance();
		
	}
	
	@GetMapping("/{idS}")
	public ResponseEntity<Seance>  findSeanceById (@PathVariable Long idS) {
		Seance seance = seanceService.findSeanceById(idS);
		if (seance==null) {
			return new ResponseEntity<Seance>(HttpStatus.NO_CONTENT);
		}else {
			return new ResponseEntity<Seance>(seance, HttpStatus.OK);
		}
	}
	
	@PostMapping
	public Seance createSeance(@RequestBody Seance seance) {
		return seanceService.createSeance(seance);
	}
	
	@PutMapping
	public Seance updateSeance(@RequestBody Seance seance) {
		return seanceService.updateSeance(seance);
				
	}
	@DeleteMapping(path= "/{idS}") 
	public void deleteEspace(@PathVariable Long idS) {
		 seanceService.deleteSeance(idS);
	}

}
