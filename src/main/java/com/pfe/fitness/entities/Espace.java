package com.pfe.fitness.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.pfe.fitness.entities.Seance;

@Entity
public class Espace implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id ;
	private String Nom;
	private String Description;
	private String Image;
	
	/*@OneToMany(mappedBy ="espace" ,cascade = {CascadeType.PERSIST,CascadeType.MERGE,CascadeType.REMOVE},fetch = FetchType.EAGER)
	private Set<Seance> seance;
	
	
	
	
	public Set<Seance> getSeance() {
		return seance;
	}
	public void setSeance(Set<Seance> seance) {
		this.seance = seance;
	}*/
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNom() {
		return Nom;
	}
	public void setNom(String nom) {
		Nom = nom;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public String getImage() {
		return Image;
	}
	public void setImage(String image) {
		Image = image;
	}
	public Espace() {
	}
	@Override
	public String toString() {
		return "Espace [id=" + id + ", Nom=" + Nom + ", Description=" + Description + ", Image=" + Image + "]";
	}
	
	public Espace(long id, String nom, String description, String image) {
		super();
		this.id = id;
		Nom = nom;
		Description = description;
		Image = image;
	}
	
	
}
