package com.pfe.fitness.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@Entity
public class Coach implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long Id ;
	private String NomPrenom;
	private String typeS;
	private String Description ;
	private String image;
	
	/*@OneToMany(mappedBy ="coach" ,cascade = {CascadeType.PERSIST,CascadeType.MERGE,CascadeType.REMOVE},fetch = FetchType.EAGER)
	private Set<Seance> seance;
	
	
	public Set<Seance> getSeance() {
		return seance;
	}
	public void setSeance(Set<Seance> seance) {
		this.seance = seance;
	}*/
	public Coach() {
	}
	public long getId() {
		return Id;
	}
	public void setId(long id) {
		Id = id;
	}
	public String getNomPrenom() {
		return NomPrenom;
	}
	public void setNomPrenom(String nomPrenom) {
		NomPrenom = nomPrenom;
	}
	
	public String getTypeS() {
		return typeS;
	}
	public void setTypeS(String typeS) {
		this.typeS = typeS;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	@Override
	public String toString() {
		return "Coach [Id=" + Id + ", NomPrenom=" + NomPrenom + ", typeS=" + typeS + ", Description=" + Description
				+ ", image=" + image + "]";
	}
	

	


}
