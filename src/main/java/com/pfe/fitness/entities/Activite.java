package com.pfe.fitness.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class Activite implements Serializable {
	    @Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private long id ;
		private String NomA;
		private String DescriptionA ;
		private String imageA;
		public long getId() {
			return id;
		}
		public void setId(long id) {
			this.id = id;
		}
		public String getNomA() {
			return NomA;
		}
		public void setNomA(String nomA) {
			NomA = nomA;
		}
		public String getDescriptionA() {
			return DescriptionA;
		}
		public void setDescriptionA(String descriptionA) {
			DescriptionA = descriptionA;
		}
		public String getImageA() {
			return imageA;
		}
		public void setImageA(String imageA) {
			this.imageA = imageA;
		}
		public Activite(long id, String nomA, String descriptionA, String imageA) {
			super();
			this.id = id;
			NomA = nomA;
			DescriptionA = descriptionA;
			this.imageA = imageA;
		}
		public Activite() {
			super();
		}
		@Override
		public String toString() {
			return "Activite [id=" + id + ", NomA=" + NomA + ", DescriptionA=" + DescriptionA + ", imageA=" + imageA
					+ "]";
		}
		
		/*	@ManyToMany(fetch = FetchType.LAZY)
	            @JoinTable(	name = "user_activite", 
				joinColumns = @JoinColumn(name = "user_id"), 
				inverseJoinColumns = @JoinColumn(name = "activite_id"))*/
		
		/*@OneToMany(mappedBy ="activite" ,cascade = {CascadeType.PERSIST,CascadeType.MERGE,CascadeType.REMOVE},fetch = FetchType.EAGER)
		private Set<Seance> seance;
		
		public Set<Seance> getSeance() {
			return seance;
		}
		public void setSeance(Set<Seance> seance) {
			this.seance = seance;
		}*/
		
		
		
		
		
		
		

}
