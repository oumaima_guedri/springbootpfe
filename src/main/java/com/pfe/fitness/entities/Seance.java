package com.pfe.fitness.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
public class Seance implements Serializable {
	 @Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private long idS ;
		private String nomS;
		private Date datedebut;
		private Date datefin;
		
		@ManyToOne
		@JsonBackReference
		@JsonIgnore
		private Espace espace;
		
		@ManyToOne
		@JsonBackReference
		@JsonIgnore
		private Activite activite;
		
		@ManyToOne
        @JsonBackReference
		@JsonIgnore
        private Coach coach;
		
		public Coach getCoach() {
			return coach;
		}
		public void setCoach(Coach coach) {
			this.coach = coach;
		}
		public Activite getActivite() {
			return activite;
		}
		public void setActivite(Activite activite) {
			this.activite = activite;
		}
		public Espace getEspace() {
			return espace;
		}
		public void setEspace(Espace espace) {
			this.espace = espace;
		}
		public long getIdS() {
			return idS;
		}
		public void setIdS(long idS) {
			this.idS = idS;
		}
		public String getNomS() {
			return nomS;
		}
		public void setNomS(String nomS) {
			this.nomS = nomS;
		}
		public Date getDatedebut() {
			return datedebut;
		}
		public void setDatedebut(Date datedebut) {
			this.datedebut = datedebut;
		}
		public Date getDatefin() {
			return datefin;
		}
		public void setDatefin(Date datefin) {
			this.datefin = datefin;
		}
		public Seance() {
			super();
		}
		
		

		
}
