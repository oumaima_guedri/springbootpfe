package com.pfe.fitness.services;


import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.pfe.fitness.entities.ERole;
import com.pfe.fitness.entities.User;
import com.pfe.fitness.repository.UserRepository;




@Service
public class UtilisateurServicesImpl implements UtilisateurServices {

	@Autowired
	private UserRepository utilisateurRespository;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	
	@Override
	public List<User> getAllUtilisateurs() {

		return utilisateurRespository.findAll();
	}

	@Override
	public User findUtilisateurById(Long id) {
		Optional<User> utOptional = utilisateurRespository.findById(id);

		if (utOptional.isEmpty()) {
			return null;
		} else {
			return utOptional.get();
		}

	}
	@Override
	public User updateUtlisateur(User utilisateur) {
		Optional<User> utOptional = utilisateurRespository.findById(utilisateur.getId());

		if (utOptional.isEmpty()) {
			return null;
		} else {
			String cryptedPassword = bCryptPasswordEncoder.encode(utilisateur.getPassword());
			utilisateur.setPassword(cryptedPassword);
			return utilisateurRespository.save(utilisateur);
		}
	}

	@Override
	public void deleteUtlisateur(Long id) {
		utilisateurRespository.deleteById(id);

	}
	
	@Override
	public User createUtlisateur(User utilisateur) {
		String cryptedPassword = bCryptPasswordEncoder.encode(utilisateur.getPassword());
		utilisateur.setPassword(cryptedPassword);
		return utilisateurRespository.save(utilisateur);
	}


	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<User> findByRolesName(ERole name) {

		return utilisateurRespository.findByRolesName(name);
	}




	
}



	
	





	


