package com.pfe.fitness.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pfe.fitness.entities.Seance;
import com.pfe.fitness.repository.SeanceRepository;

@Service
public class SeanceServiceImp implements SeanceService {

	@Autowired
	private SeanceRepository seanceRepository ;
	
	@Override
	public List<Seance> getAllSeance() {

		return seanceRepository.findAll();
	}

	@Override
	public Seance findSeanceById(Long idS) {
		Optional<Seance> seOptional= seanceRepository.findById(idS);
		if (seOptional.isEmpty()) {
			return null;
		} else {
			return seOptional.get();
		}
	}

	@Override
	public Seance createSeance(Seance seance) {
		return seanceRepository.save(seance);

	}
	
	@Override
	public Seance updateSeance(Seance seance) {
		Optional<Seance> seOptional = seanceRepository.findById(seance.getIdS());

		if (seOptional.isEmpty()) {
			return null;
		} else {
			return seanceRepository.save(seance);
		}
	}



	@Override
	public void deleteSeance(Long idS) {
		seanceRepository.deleteById(idS);

	}
}
