package com.pfe.fitness.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pfe.fitness.entities.Activite;
import com.pfe.fitness.entities.Espace;
import com.pfe.fitness.entities.Events;
import com.pfe.fitness.repository.EspaceRepository;


@Service
public class EspaceServiceImpl implements EspaceService {

	@Autowired
	private EspaceRepository espaceRepository ;
	
	@Override
	public List<Espace> getAllEspace() {

		return espaceRepository.findAll();
	}

	@Override
	public Espace findEspaceById(Long id) {
		Optional<Espace> esOptional= espaceRepository.findById(id);
		if (esOptional.isEmpty()) {
			return null;
		} else {
			return esOptional.get();
		}
	}

	@Override
	public Espace createEspace(Espace espace) {
		return espaceRepository.save(espace);

	}
	
	@Override
	public Espace updateEspace(Espace espace) {
		Optional<Espace> utOptional = espaceRepository.findById(espace.getId());

		if (utOptional.isEmpty()) {
			return null;
		} else {
			return espaceRepository.save(espace);
		}
	}



	@Override
	public void deleteEspace(Long id) {
		espaceRepository.deleteById(id);

	}



}
