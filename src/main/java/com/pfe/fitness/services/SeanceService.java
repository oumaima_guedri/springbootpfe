package com.pfe.fitness.services;

import java.util.List;

import com.pfe.fitness.entities.Seance;


public interface SeanceService {
	public List<Seance> getAllSeance();
	public Seance findSeanceById(Long idS);
	public Seance createSeance(Seance seance);
	public void  deleteSeance(Long idS);
	public Seance updateSeance(Seance seance);

}
