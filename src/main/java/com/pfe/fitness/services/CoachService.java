package com.pfe.fitness.services;

import java.util.List;

import com.pfe.fitness.entities.Coach;


public interface CoachService {
	public List<Coach> getAllCoach();
	public Coach findCoachById (Long id);
	public Coach createCoach( Coach coach);
	public Coach updateCoach( Coach coach);
	public void deleteCoach( long id);
	public String deleteCoach(String nomC);


}
