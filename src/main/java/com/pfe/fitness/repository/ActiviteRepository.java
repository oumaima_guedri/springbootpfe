package com.pfe.fitness.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pfe.fitness.entities.Activite;


@Repository
public interface ActiviteRepository extends JpaRepository<Activite, Long> {

}
