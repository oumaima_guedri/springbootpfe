package com.pfe.fitness.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pfe.fitness.entities.Seance;

@Repository
public interface SeanceRepository extends JpaRepository<Seance, Long> {

}
